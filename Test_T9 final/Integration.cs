﻿using NUnit.Framework;
using T9_final;

namespace T9.Test
{
    [TestFixture]
    public class Integration 
    {
        [Test]
        public void Test1Word()
        {
            string result = Convertor.Convertation("hi");
            Assert.AreEqual("44 444", result);
        }

        [Test]
        public void Test2Word()
        {
            string result = Convertor.Convertation("yes");
            Assert.AreEqual("999337777", result);
        }

        [Test]
        public void Test3Word()
        {
            string result = Convertor.Convertation("foo  bar");
            Assert.AreEqual("333666 6660 022 2777", result);
        }

        [Test]
        public void Test4Word()
        {
            string result = Convertor.Convertation("hello world");
            Assert.AreEqual("4433555 555666096667775553", result);
        }

        [Test]
        public void TestDigit()
        {
            string result = Convertor.Convertation("1");
            Assert.AreEqual("1", result);
        }

        [Test]
        public void TestEmptyString()
        {
            string result = Convertor.Convertation("");
            Assert.AreEqual("", result);
        }
    }
}