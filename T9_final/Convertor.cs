﻿using System;
using System.IO;


namespace T9_final
{
    public class Convertor
    {

        private static readonly char[] Zero = { ' ', '0' };
        private static readonly char[] One = { '1' };
        private static readonly char[] Two = { 'a', 'b', 'c', '2' };
        private static readonly char[] Three = { 'd', 'e', 'f', '3' };
        private static readonly char[] Four = { 'g', 'h', 'i', '4' };
        private static readonly char[] Five = { 'j', 'k', 'l', '5' };
        private static readonly char[] Six = { 'm', 'n', 'o', '6' };
        private static readonly char[] Seven = { 'p', 'q', 'r', 's', '7' };
        private static readonly char[] Eight = { 't', 'u', 'v', '8' };
        private static readonly char[] Nine = { 'w', 'x', 'y', 'z', '9' };

        private static readonly char[][] Keyboard = { Zero, One, Two, Three, Four, Five, Six, Seven, Eight, Nine };

        public static string Convertation(string input)
        {
            string result = "";
            for (int i = 0; i < input.Length; i++)
            {
                if (i != 0 && IsFromSameButton(input[i], input[i - 1]))
                {
                    result += " ";
                }
                result += FromButton(input[i]);
            }
            return result;
        }

        private static string FromButton(char a)
        {
            string result = "";
            for (int i = 0; i < Keyboard.Length; i++) // Проверяем каждую цифу клавиатуры
            {
                for (int j = 0; j < Keyboard[i].Length; j++) // Проверяем все буквы под i-ой цифрой
                {
                    if (a == Keyboard[i][j]) // Если j-ая буква под i-ой цифорй клавиатуры == данному символу (а), то...
                    {
                        result = WriteIntNTimes(i, j + 1); // Дописать в результат j + 1 раз i-ую цифру
                    }
                }
            }
            
            return result;
        }

        private static string WriteIntNTimes(int a, int n)
        {
            string result = "";
            for (int i = 0; i < n; i++)
            {
                result += a;
            }

            return result;
        }

        private static bool IsFromSameButton(char a, char b)
        {
            return FromButton(a)[0] == FromButton(b)[0];
        }
    }
}
