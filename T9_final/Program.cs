﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T9_final
{
    class Program
    {
        static void Main(string[] args)
        {
            FileStream file1 =
                new FileStream(".\\C-large-practice.in", FileMode.Open); //создаем файловый поток
            StreamReader
                reader = new StreamReader(file1); // создаем «потоковый читатель» и связываем его с файловым потоком 

            int numOfInputs = int.Parse(reader.ReadLine());
            string[] outputs = new string[numOfInputs];
            for (int i = 0; i < numOfInputs; i++)
            {
                string input = reader.ReadLine();
                outputs[i] = Convertor.Convertation(input);
            }


            FileStream file2 =
                new FileStream(".\\Output.txt", FileMode.Create);
            StreamWriter
                writer = new StreamWriter(file2);
            for (int i = 0; i < numOfInputs; i++)
            {
                writer.WriteLine("Case #" + (i + 1) + ": " + outputs[i]);
            }

            Console.WriteLine("Done!");
            Console.Read();

            writer.Close();
            reader.Close();
        }
    }
}